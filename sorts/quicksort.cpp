#include <iostream>
#include <vector>

std::pair<int*, int*> partition(int* l, int* r, int pivot) {
    int* equal = l;
    int* greater = l;
    int* now = l;
    while (now < r) {
        if (*now > pivot) {
            now++;
        } else if (*now == pivot) {
            *now = *greater;
            *greater = pivot;
            greater++;
            now++;
        } else {
            int help = *now;
            *now = *greater;
            *greater = *equal;
            *equal = help;
            equal++;
            greater++;
            now++;
        }
    }
    return {equal, greater};
}

void quick_sort(int* l, int* r) {
    if (l + 1 == r || l == r) {
        return;
    }
    int num = rand() % (r - l);
    int pivot = l[num];
    auto divider = partition(l, r, pivot);
    quick_sort(l, divider.first);
    quick_sort(divider.second, r);
}

int main() {
    int N;
    std::cin >> N;
    int *a = new int[N];
    for (int i = 0; i < N; i++) {
        std::cin >> a[i];
    }
    quick_sort(a, a + N);
    for (int i = 0; i < N; i++) {
        std::cout << a[i] << " ";
    }
    delete[] a;
    return 0;
}
