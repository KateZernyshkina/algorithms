#include <iostream>
#include <vector>

void get_buckets(std::vector<std::string>& array, std::vector<std::vector<std::string>>& buckets, std::vector<int>& index, size_t num) {
    for (auto elem : array) {
        buckets[elem[num] - '0'].push_back(elem);
        index[elem[num] - '0']++;
    }
    for (int i = 9; i >= 0; i--) {
        index[i] = index[i - 1];
    }
    index[0] = 0;
    for (int i = 1; i < 10; i++) {
        index[i] = index[i - 1] + index[i];
    }
    for (int i = 0; i < 10; i++) {
        std::cout << "Bucket " << i << ": ";
        if (buckets[i].empty()) {
            std::cout << "empty\n";
        } else {
            auto size = buckets[i].size();
            for (size_t j = 0; j < size; j++) {
                if (j != size - 1) {
                    std::cout << buckets[i][j] << ", ";
                } else {
                    std::cout << buckets[i][j] << "\n";
                }
            }
        }
    }
}

int main() {
    int N;
    std::cin >> N;
    std::vector<std::string> array(N);
    std::vector<std::string> array_copy(N);
    std::vector<std::vector<std::string>> buckets(10);
    std::vector<int> index(10, 0);
    for (int i = 0; i < N; i++) {
        std::cin >> array[i];
    }
    std::cout << "Initial array:\n";
    for (int j = 0; j < N; j++) {
        if (j != N - 1) {
            std::cout << array[j] << ", ";
        } else {
            std::cout << array[j];
        }
    }
    std::cout << "\n**********\n";
    if (N != 0) {
        size_t len = array[0].size();
        for (size_t i = 0; i < len; i++) {
            std::cout << "Phase " << i + 1 << "\n";
            get_buckets(array, buckets, index, len - i - 1);
            for (int j = 0; j < N; j++) {
                array_copy[index[array[j][len - i - 1] - '0']] = array[j];
                index[array[j][len - i - 1] - '0']++;
            }
            std::swap(array, array_copy);
            for (int j = 0; j < 10; j++) {
                buckets[j].clear();
            }
            std::fill(index.begin(), index.end(), 0);
            std::cout << "**********\n";
        }
    }
    std::cout << "Sorted array:\n";
    for (int j = 0; j < N; j++) {
        if (j != N - 1) {
            std::cout << array[j] << ", ";
        } else {
            std::cout << array[j];
        }
    }
    std::cout << std::endl;
    return 0;
}
