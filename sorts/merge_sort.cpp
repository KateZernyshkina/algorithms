#include <iostream>
#include <vector>

void merge(int* l1, int* r1, int* l2, int* r2, int* res) {
    while (l1 < r1 && l2 < r2) {
        if (*l1 < *l2) {
            *res = *l1;
            l1++;
            res++;
        } else {
            *res = *l2;
            l2++;
            res++;
        }
    }
    while (l1 < r1) {
        *res = *l1;
        l1++;
        res++;
    }
    while (l2 < r2) {
        *res = *l2;
        l2++;
        res++;
    }
}

void merge_sort(int* l1, int* r1, int* l2, int* r2) {
    if (l1 == r1) {
        return;
    }
    if (l1 + 1 == r1) {
        *l2 = *l1;
        return;
    }
    int size = (r1 - l1) / 2;

    merge_sort(l1, l1 + size, l2, l2 + size);
    merge_sort(l1 + size, r1, l2 + size, r2);

    merge(l2, l2 + size, l2 + size, r2, l1);
    for (int i = 0; i < r1 - l1; i++) {
        l2[i] = l1[i];
    }
}

int main() {
    int N;
    std::cin >> N;
    int *a = new int[N];
    int *b = new int[N];
    for (int i = 0; i < N; i++) {
        std::cin >> a[i];
    }
    merge_sort(a, a + N, b, b + N);
    for (int i = 0; i < N; i++) {
        std::cout << b[i] << " ";
    }
    delete[] a;
    delete[] b;
    return 0;
}
