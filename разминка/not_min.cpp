#include <iostream>
#include <vector>

void not_min(std::vector<int>& a, int l, int r) {
    int min = a[l];
    for (int i = l + 1; i <= r; i++) {
        if (a[i] > min) {
            std::cout << a[i] << std::endl;
            return;
        } else if (a[i] < min) {
            std::cout << min << std::endl;
            return;
        }
    }
    std::cout << "NOT FOUND\n";
}

int main() {
    int N, M, l, r;
    std::cin >> N >> M;
    std::vector<int> a(N);
    for (int i = 0; i < N; i++) {
        std::cin >> a[i];
    }
    for (int i = 0; i < M; i++) {
        std::cin >> l >> r;
        not_min(a, l, r);
    }
    return 0;
}
