#include <iostream>
#include <vector>

int main() {
    std::vector<int> letters(26, 0);
    std::string s1, s2;
    std::cin >> s1 >> s2;
    for (auto elem: s1) {
        letters[elem - 'a']++;
    }
    for (auto elem: s2) {
        letters[elem - 'a']--;
    }
    for (auto number : letters) {
        if (number != 0) {
            std::cout << "NO\n";
            return 0;
        }
    }
    std::cout << "YES\n";
    return 0;
}
