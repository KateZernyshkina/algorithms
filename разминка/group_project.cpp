#include <iostream>

int main() {
    int64_t t, a, b, n, groups;
    std::cin >> t;
    for (int i = 0; i < t; i++) {
        std::cin >> n >> a >> b;
        groups = n / a;
        if ((b - a) * groups >= n % a) {
            std::cout << "YES\n";
        } else {
            std::cout << "NO\n";
        }
    }
    return 0;
}
