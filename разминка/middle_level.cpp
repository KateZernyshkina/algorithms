#include <iostream>
#include <vector>

int main() {
    int N;
    std::cin >> N;
    std::vector<int> a(N);
    std::vector<int> res(N, 0);
    for (int i = 0; i < N; i++) {
        std::cin >> a[i];
        res[0] += a[i] - a[0];
    }
    for (int i = 1; i < N; i++) {
        res[i] = res[i - 1] + (a[i] - a[i - 1]) * (i - 1 - (N - i - 1));
    }
    for (auto elem : res) {
        std::cout << elem << " ";
    }
    return 0;
}
