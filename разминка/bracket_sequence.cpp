#include <iostream>
#include <string>
#include <vector>

int main() {
    std::string string;
    std::vector<char> bracket;
    std::cin >> string;
    for (auto elem : string) {
        if (elem == '(' || elem == '[' || elem == '{') {
            bracket.push_back(elem);
        } else if (!bracket.empty() && (elem == ')' && bracket.back() == '(' ||
                   elem == ']' && bracket.back() == '[' ||
                   elem == '}' && bracket.back() == '{')) {
            bracket.pop_back();
        } else {
            std::cout << "no\n";
            return 0;
        }
    }
    if (bracket.empty()) {
        std::cout << "yes\n";
    } else {
        std::cout << "no\n";
    }
    return 0;
}
