#include <iostream>

int main() {
    int a, b, c, d, m, n;
    std::cin >> a >> b >> c >> d;
    m = a * d + b * c;
    n = b * d;
    a = m;
    b = n;
    while (a != 0 && b != 0) {
        if (a > b) {
            a = a % b;
        } else {
            b = b % a;
        }
    }
    std::cout << m / (a + b) << " " << n / (a + b);
    return 0;
}
