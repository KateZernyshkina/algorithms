#include <iostream>

int main() {
    int a, b, n;
    std::cin >> a >> b >> n;
    if (a > (b + n - 1) / n) {
        std::cout << "Yes\n";
    } else {
        std::cout << "No\n";
    }
    return 0;
}
