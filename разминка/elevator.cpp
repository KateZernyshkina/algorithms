#include <iostream>
#include <cstdint>
#include <vector>

int main() {
    uint64_t k, n;
    uint64_t t = 0;
    std::cin >> k >> n;
    std::vector<uint64_t> a(n);
    for (int i = 0; i < n; i++) {
        std::cin >> a[i];
    }
    uint64_t last = n - 1;
    while (last != -1) {
        if (a[last] == 0) {
            last--;
        } else {
            t += a[last] / k * 2 * (last + 1);
            a[last] = a[last] % k;
            if (a[last] != 0) {
                t += 2 * (last + 1);
                uint64_t new_k = k;
                while (new_k != 0 && last != -1) {
                    if (new_k > a[last]) {
                        new_k -= a[last];
                        a[last] = 0;
                        last--;
                    } else if (new_k < a[last]) {
                        a[last] -= new_k;
                        new_k = 0;
                    } else {
                        a[last] = 0;
                        new_k = 0;
                        last--;
                    }
                }
            } else {
                last--;
            }
        }
    }
    std::cout << t;
    return 0;
}
