#include <iostream>
#include <cmath>

double rad(int64_t x, int64_t y) {
    return sqrt(x * x + y * y);
}

int main() {
    int64_t x_a, y_a, x_b, y_b;
    const double pi = 3.141592653589793;
    std::cin >> x_a >> y_a >> x_b >> y_b;
    double r_a = rad(x_a, y_a);
    double r_b = rad(x_b, y_b);
    // std::cout << r_a << " " << r_b << "\n";
    if (r_b < r_a) {
        std::swap(r_a, r_b);
    }
    double s = r_a + r_b;
    double phi_1 = atan2(y_a, x_a);
    double phi_2 = atan2(y_b, x_b);
    // std::cout << phi_1 << " " << phi_2 << "\n";
    if (phi_2 < phi_1) {
        std::swap(phi_1, phi_2);
    }
    double delta = std::min(phi_2 - phi_1, 2 * pi - phi_2 + phi_1);
    s = std::min(s, r_b - r_a + delta * r_a);
    printf("%.12lf\n", s);
    return 0;
}
