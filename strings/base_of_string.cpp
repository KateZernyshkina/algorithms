#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

bool is_equal(int64_t p, int l, int a, int b, std::vector<int64_t>& powers, std::vector<int64_t>& hash) {
    if ((hash[a + l - 1] + hash[b - 1] * powers[l]) % p ==
        (hash[b + l - 1] + hash[a - 1] * powers[l]) % p) {
        return true;
    }
    return false;
}

int main() {
    std::string s;
    int64_t x = 257;
    int64_t p = pow(10, 9) + 7;
    std::cin >> s;
    int len = s.length() + 1;
    std::vector<int64_t> powers(len, 1);
    std::vector<int64_t> hash(len, 0);
    s = " " + s;
    for (int i = 1; i < len; i++) {
        powers[i] = (powers[i - 1] * x) % p;
        hash[i] = (hash[i - 1] * x + s[i] - 'a') % p;
    }
    for (int i = 2; i < len; i++) {
        if (is_equal(p, len - i, 1, i, powers, hash)) {
            std::cout << i - 1 << std::endl;
            return 0;
        }
    }
    std::cout << len - 1 << std::endl;
    return 0;
}
