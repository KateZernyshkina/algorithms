#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

int main() {
    int64_t x = 1000037;
    int64_t p = pow(10, 9) + 7;
    int n, m;
    std::cin >> n >> m;
    std::vector<int> cubes(n + 1);
    for (int i = 1; i < n + 1; i++) {
        std::cin >> cubes[i];
    }
    std::vector<int64_t> powers(n + 1, 1);
    std::vector<int64_t> hash(n + 1, 0);
    std::vector<int64_t> rev_hash(n + 2, 0);
    for (int i = 1; i < n + 1; i++) {
        powers[i] = (powers[i - 1] * x) % p;
        hash[i] = (hash[i - 1] * x + cubes[i]) % p;
        rev_hash[n + 1 - i] = (rev_hash[n + 2 - i] * x + cubes[n + 1 - i]) % p;
    }
    for (int i = n / 2; i >= 0; i--) {
        if ((hash[2 * i] + rev_hash[i + 1] * powers[i]) % p ==
                (rev_hash[1] + hash[i] * powers[i]) % p) {
            std::cout << n - i << " ";
        }
    }
    return 0;
}
