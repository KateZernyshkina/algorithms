#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

int main() {
    std::string s;
    std::cin >> s;
    int len = s.length();
    std::vector<int> z_f(len);
    z_f[0] = 0;
    int left = 0, right = 0;
    for (int i = 1; i < len; i++) {
        z_f[i] = std::max(0, std::min(right - i, z_f[i - left]));
        while (i + z_f[i] < len && s[z_f[i]] == s[i + z_f[i]]) {
            z_f[i]++;
        }
        if (i + z_f[i] > right) {
            left = i;
            right = i + z_f[i];
        }
    }
    for (auto elem : z_f) {
        std::cout << elem << " ";
    }
    return 0;
}
