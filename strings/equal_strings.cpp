#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

bool is_equal(int64_t p, int l, int a, int b, std::vector<int64_t>& powers, std::vector<int64_t>& hash) {
    if ((hash[a + l - 1] + hash[b - 1] * powers[l]) % p ==
        (hash[b + l - 1] + hash[a - 1] * powers[l]) % p) {
        return true;
    }
    return false;
}

int main() {
    std::string s;
    int q, l, a, b;
    int64_t x = 257;
    int64_t p = pow(10, 9) + 7;
    std::cin >> s >> q;
    auto len = s.length() + 1;
    std::vector<int64_t> powers(len, 1);
    std::vector<int64_t> hash(len, 0);
    s = " " + s;
    for (int i = 1; i < len; i++) {
        powers[i] = (powers[i - 1] * x) % p;
        hash[i] = (hash[i - 1] * x + s[i] - 'a') % p;
    }
    for (int i = 0; i < q; i++) {
        std::cin >> l >> a >> b;
        if (is_equal(p, l, a + 1, b + 1, powers, hash)) {
            std::cout << "yes\n";
        } else {
            std::cout << "no\n";
        }
    }
    return 0;
}
