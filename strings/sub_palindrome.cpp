#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>

bool is_palindrome(int l, int begin, int64_t p, std::vector<int64_t>& hash, std::vector<int64_t>& rev_hash, std::vector<int64_t>& powers) {
    if ((hash[begin - 1 + l] + rev_hash[begin + l] * powers[l]) % p ==
        (rev_hash[begin] + hash[begin - 1] * powers[l]) % p) {
        return true;
    }
    return false;
}

int main() {
    int64_t x = 257;
    int64_t p = pow(10, 9) + 7;
    std::string s;
    std::cin >> s;
    int len = s.length() + 1;
    s = ' ' + s;
    std::vector<int64_t> powers(len, 1);
    std::vector<int64_t> hash(len, 0);
    std::vector<int64_t> rev_hash(len + 1, 0);
    for (int i = 1; i < len; i++) {
        powers[i] = (powers[i - 1] * x) % p;
        hash[i] = (hash[i - 1] * x + s[i] - 'a' + 1) % p;
        rev_hash[len - i] = (rev_hash[len + 1 - i] * x + s[len - i] - 'a' + 1) % p;
    }
    int n_palindrome = len - 1;
    int l, r, rad;
    for (int i = 2; i < len - 1; i++) {
        l = 0;
        r = std::min(i - 1, len - i - 1);
        while (l < r - 1) {
            rad = (l + r) / 2;
            if (is_palindrome(1 + 2 * rad, i - rad, p, hash, rev_hash, powers)) {
                l = rad;
            } else {
                r = rad;
            }
        }
        if (is_palindrome(1 + 2 * r, i - r, p, hash, rev_hash, powers)) {
            l = r;
        }
        n_palindrome += l;
    }

    for (int i = 1; i < len - 1; i++) {
        l = 0;
        r = std::min(i, len - i - 1);
        while (l < r - 1) {
            rad = (l + r) / 2;
            if (is_palindrome(2 * rad, i - rad + 1, p, hash, rev_hash, powers)) {
                l = rad;
            } else {
                r = rad;
            }
        }
        if (is_palindrome(2 * r, i - r + 1, p, hash, rev_hash, powers)) {
            l = r;
        }
        n_palindrome += l;
    }

    std::cout << n_palindrome;
    return 0;
}
