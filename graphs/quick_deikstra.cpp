#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <map>

int main() {
    int n, k, s, f, dist;
    std::cin >> n >> k;
    std::vector<std::vector<std::pair<int, int64_t>>> edges(n + 1);
    std::vector<int64_t> dists(n + 1, -1);
    std::map<std::pair<int64_t, int>, int> map_dists;
    std::vector<bool> is_done(n + 1, false);
    for (int i = 0; i < k; i++) {
        std::cin >> s >> f >> dist;
        edges[s].push_back(std::pair(f, dist));
        edges[f].push_back(std::pair(s, dist));
    }
    std::cin >> s >> f;
    dists[s] = 0;
    map_dists[{0, s}] = s;
    while (true) {
        if (map_dists.empty()) {
            break;
        }
        int next = map_dists.begin()->second;
        int64_t next_dist = map_dists.begin()->first.first;
        map_dists.erase({next_dist, next});
        is_done[next] = true;
        for (auto elem: edges[next]) {
            auto vertex = elem.first;
            if (!is_done[vertex]) {
                auto cur_dist = elem.second;
                if (dists[vertex] == -1) {
                    dists[vertex] = dists[next] + cur_dist;
                    map_dists[{dists[vertex], vertex}] = vertex;
                } else if (dists[vertex] > dists[next] + cur_dist) {
                    map_dists.erase({dists[vertex], vertex});
                    dists[vertex] = dists[next] + cur_dist;
                    map_dists[{dists[vertex], vertex}] = vertex;
                }
            }
        }
    }
    std::cout << dists[f];
    return 0;
}
