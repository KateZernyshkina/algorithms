#include <cstdint>
#include <iostream>
#include <vector>

int main() {
    int n, s, f;
    std::cin >> n >> s >> f;
    std::vector<std::vector<std::pair<int, int>>> edges(n + 1);
    std::vector<int> dists(n + 1, -1);
    std::vector<bool> is_done(n + 1, false);
    dists[s] = 0;
    int dist;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            std::cin >> dist;
            if (dist != -1 && i != j) {
                edges[i].push_back(std::pair(j, dist));
            }
        }
    }
    while (true) {
        int min_dist = 1000000000;
        int next = -1;
        for (int i = 1; i <= n; i++) {
            if (dists[i] != -1 && !is_done[i] && dists[i] < min_dist) {
                next = i;
                min_dist = dists[i];
            }
        }
        if (next == -1) {
            break;
        }
        is_done[next] = true;
        for (auto elem: edges[next]) {
            auto vertex = elem.first;
            auto cur_dist = elem.second;
            if (dists[vertex] == -1 || dists[vertex] > dists[next] + cur_dist) {
                dists[vertex] = dists[next] + cur_dist;
            }
        }
    }
    std::cout << dists[f];
    return 0;
}
