#include <cstdint>
#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

int main() {
    int n;
    std::cin >> n;
    std::vector<std::pair<int, int>> towns(n + 1);
    for (int i = 1; i < n + 1; i++) {
        std::cin >> towns[i].first >> towns[i].second;
    }
    std::vector<std::vector<std::pair<int, double>>> edges(n + 1);
    std::vector<std::vector<std::pair<int, double>>> all_edges(n + 1);
    int a, b;
    double dist;
    for (int i = 0; i < n - 1; i++) {
        std::cin >> a >> b >> dist;
        edges[a].push_back(std::pair(b, dist));
        edges[b].push_back(std::pair(a, dist));
    }
    //std::vector<std::vector<int>> before(n + 1, std::vector<int> (n + 1, -1));
    for (int i = 1; i < n + 1; i++) {
        std::map<std::pair<double, int>, int> map_dists;
        std::vector<bool> is_done(n + 1, false);
        std::vector<double> dists(n + 1, -1);
        dists[i] = 0;
        map_dists[{0, i}] = i;
        while (true) {
            if (map_dists.empty()) {
                break;
            }
            int next = map_dists.begin()->second;
            double next_dist = map_dists.begin()->first.first;
            map_dists.erase({next_dist, next});
            is_done[next] = true;
            if (next != i) {
                all_edges[i].push_back({next, dists[next]});
            }
            for (auto elem: edges[next]) {
                auto vertex = elem.first;
                if (!is_done[vertex]) {
                    auto cur_dist = elem.second;
                    if (dists[vertex] == -1) {
                        dists[vertex] = dists[next] + cur_dist;
                        map_dists[{dists[vertex], vertex}] = vertex;
                        //before[i][vertex] = next;
                    } else if (dists[vertex] > dists[next] + cur_dist) {
                        map_dists.erase({dists[vertex], vertex});
                        dists[vertex] = dists[next] + cur_dist;
                        map_dists[{dists[vertex], vertex}] = vertex;
                        //before[i][vertex] = next;
                    }
                }
            }
        }
    }
    std::map<std::pair<double, int>, int> map_dists;
    std::vector<bool> is_done(n + 1, false);
    std::vector<double> dists(n + 1, -1);
    std::vector<int> prev(n + 1, -1);
    dists[1] = 0;
    map_dists[{0, 1}] = 1;
    while (true) {
        if (map_dists.empty()) {
            break;
        }
        int next = map_dists.begin()->second;
        double next_dist = map_dists.begin()->first.first;
        map_dists.erase({next_dist, next});
        is_done[next] = true;
        for (auto elem: all_edges[next]) {
            auto vertex = elem.first;
            if (!is_done[vertex]) {
                auto cur_dist = towns[vertex].first + elem.second / towns[vertex].second;
                //std::cout << next << " " << vertex << " " << cur_dist << " " << dists[vertex] << "\n";
                if (dists[vertex] == -1) {
                    dists[vertex] = dists[next] + cur_dist;
                    map_dists[{dists[vertex], vertex}] = vertex;
                    prev[vertex] = next;
                } else if (dists[vertex] > dists[next] + cur_dist) {
                    map_dists.erase({dists[vertex], vertex});
                    dists[vertex] = dists[next] + cur_dist;
                    map_dists[{dists[vertex], vertex}] = vertex;
                    prev[vertex] = next;
                }
            }
        }
    }
    double max = 0;
    int last = 1;
    for (int i = 2; i < n + 1; i++) {
        if (dists[i] > max) {
            max = dists[i];
            last = i;
        }
    }
    printf("%.10lf\n", max);
    while (last != -1) {
        printf("%d ", last);
        last = prev[last];
    }
    return 0;
}
