#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <map>

int main() {
    int n, k, s, f;
    std::cin >> n >> s >> f >> k;
    std::vector<std::vector<std::pair<int, std::pair<int, int>>>> edges(n + 1);
    std::vector<int> dists(n + 1, -1);
    std::map<std::pair<int, int>, int> map_dists;
    std::vector<bool> is_done(n + 1, false);
    int s1, t1, f1, t2;
    for (int i = 0; i < k; i++) {
        std::cin >> s1 >> t1 >> f1 >> t2;
        edges[s1].push_back({f1, {t1, t2}});
    }
    dists[s] = 0;
    map_dists[{0, s}] = s;
    while (true) {
        if (map_dists.empty()) {
            break;
        }
        int next = map_dists.begin()->second;
        int next_dist = map_dists.begin()->first.first;
        map_dists.erase({next_dist, next});
        is_done[next] = true;
        for (auto elem: edges[next]) {
            auto vertex = elem.first;
            auto cur_t1 = elem.second.first;
            auto cur_t2 = elem.second.second;
            if (!is_done[vertex] && cur_t1 >= dists[next]) {
                if (dists[vertex] == -1) {
                    dists[vertex] = cur_t2;
                    map_dists[{dists[vertex], vertex}] = vertex;
                } else if (dists[vertex] > cur_t2) {
                    map_dists.erase({dists[vertex], vertex});
                    dists[vertex] = cur_t2;
                    map_dists[{dists[vertex], vertex}] = vertex;
                }
            }
        }
    }
    std::cout << dists[f];
    return 0;
}
