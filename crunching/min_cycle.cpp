#include <cstdint>
#include <iostream>
#include <vector>

void find_way(std::vector<bool> &is_visited, std::vector<int> &min_edge,
              std::vector<std::vector<std::pair<int, int>>> &edges, int min,
              int cur_ver, int remain, int& min_dist, int cur_dist) {
    if (remain == 0) {
        if (cur_ver == 1) {
            min_dist = 0;
            return;
        }
        for (auto edge:edges[cur_ver]) {
            if (edge.first == 1) {
                cur_dist += edge.second;
                if (cur_dist < min_dist || min_dist == -1) {
                    min_dist = cur_dist;
                }
                return;
            }
        }
        return;
    }
    for (auto edge:edges[cur_ver]) {
        if (!is_visited[edge.first] && (min_dist == -1 || cur_dist + min < min_dist)) {
            is_visited[edge.first] = true;
            find_way(is_visited, min_edge, edges, min - min_edge[cur_ver], edge.first, remain - 1, min_dist, cur_dist + edge.second);
            is_visited[edge.first] = false;
        }
    }
}

int main() {
    int n;
    std::cin >> n;
    std::vector<std::vector<std::pair<int, int>>> edges(n + 1);
    std::vector<bool> is_visited(n + 1, false);
    std::vector<int> min_edge(n + 1, 0);
    is_visited[1] = true;
    int dist, min = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            std::cin >> dist;
            if (dist > 0) {
                edges[i].push_back({j, dist});
                if (dist < min_edge[i] || min_edge[i] == 0) {
                    min_edge[i] = dist;
                }
            }
        }
        min += min_edge[i];
    }
    int min_dist = -1;
    find_way(is_visited, min_edge, edges, min, 1, n - 1, min_dist, 0);
    std::cout << min_dist << "\n";
    return 0;
}
