#include <cstdint>
#include <iostream>
#include <vector>

void count_combination(int n, int cur_h, std::vector<bool> &w, std::vector<bool> &d1,
                       std::vector<bool> &d2, int &count) {
    if (cur_h == n) {
        count++;
        return;
    }
    for (int j = 0; j < n; j++) {
        if (!w[j] && !d1[cur_h + j] && !d2[cur_h - j + n - 1]) {
            w[j] = true;
            d1[cur_h + j] = true;
            d2[cur_h - j + n - 1] = true;
            count_combination(n, cur_h + 1, w, d1, d2, count);
            w[j] = false;
            d1[cur_h + j] = false;
            d2[cur_h - j + n - 1] = false;
        }
    }
}

int main() {
    int n, count = 0;
    std::cin >> n;
    std::vector<bool> d1(2 * n - 1, false);
    std::vector<bool> d2(2 * n - 1, false);
    std::vector<bool> w(n, false);
    count_combination(n, 0, w, d1, d2, count);
    std::cout << count;
    return 0;
}
