#include <cstdint>
#include <iostream>
#include <vector>

void get_sequence(std::vector<char>& stack, std::vector<char>& sequence, int cur_pos, int n) {
   if (n == 0) {
       for (auto elem: sequence) {
           std::cout << elem;
       }
       std::cout << "\n";
   } else {
       if (n - 1 == stack.size()) {
           return;
       }
       if (n > stack.size()) {
           sequence[cur_pos] = '(';
           stack.push_back('(');
           get_sequence(stack, sequence, cur_pos + 1, n - 1);
           stack.pop_back();

           sequence[cur_pos] = '[';
           stack.push_back('[');
           get_sequence(stack, sequence, cur_pos + 1, n - 1);
           stack.pop_back();
       }
       if (!stack.empty()) {
           auto symbol = stack.back();
           stack.pop_back();
           if (symbol == '(') {
               sequence[cur_pos] = ')';
               get_sequence(stack, sequence, cur_pos + 1, n - 1);
               stack.push_back('(');
           } else {
               sequence[cur_pos] = ']';
               get_sequence(stack, sequence, cur_pos + 1, n - 1);
               stack.push_back('[');
           }
       }
   }
}

int main() {
    int n;
    std::cin >> n;
    std::vector<char> stack;
    std::vector<char> sequence(n);
    get_sequence(stack, sequence, 0, n);
    return 0;
}
