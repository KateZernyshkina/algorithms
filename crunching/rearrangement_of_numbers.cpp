#include <cstdint>
#include <iostream>
#include <vector>

void print_numbers(int n, std::vector<bool>& exists, int cur_number) {
    if (n == 0) {
        std::cout << cur_number << "\n";
        return;
    }
    for (int i = 0; i < exists.size(); i++) {
        if (!exists[i]) {
            exists[i] = true;
            print_numbers(n - 1, exists, 10 * cur_number + i + 1);
            exists[i] = false;
        }
    }
}

int main() {
    int n;
    std::cin >> n;
    std::vector<bool> exists(n, false);
    print_numbers(n, exists, 0);
    return 0;
}
