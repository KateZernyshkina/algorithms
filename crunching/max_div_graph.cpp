#include <cstdint>
#include <iostream>
#include <vector>

void move_vertex(int n, std::vector<int>& vertexes, std::vector<int>& max_vertexes,
                 std::vector<std::vector<std::pair<int, int>>>& edges, int& max, int cur_ver, int cur_val) {
    int cur_max = cur_val;
    for (int i = cur_ver; i <= n; i++) {
        vertexes[i] = 2;
        for (auto edge: edges[i]) {
            if (vertexes[edge.first] == 2) {
                cur_max -= edge.second;
            } else {
                cur_max += edge.second;
            }
        }
        if (cur_max > max) {
            max = cur_max;
            for (int j = 1; j <= n; j++) {
                max_vertexes[j] = vertexes[j];
            }
        }
        move_vertex(n, vertexes, max_vertexes ,edges, max, i + 1, cur_max);
        vertexes[i] = 1;
        cur_max = cur_val;
    }
}

int main() {
    int n;
    std::cin >> n;
    std::vector<std::vector<std::pair<int, int>>> edges(n + 1);
    std::vector<int> vertexes(n + 1, 1);
    std::vector<int> max_vertexes(n + 1, 1);
    vertexes[1] = 2;
    max_vertexes[1] = 2;
    int dist, cur_ver = 2;
    int max = 0;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            std::cin >> dist;
            if (dist > 0) {
                edges[i].push_back({j, dist});
                if (i == 1) {
                    max += dist;
                }
            }
        }
    }
    move_vertex(n, vertexes, max_vertexes, edges, max, cur_ver, max);
    std::cout << max << "\n";
    for (int i = 1; i <= n; i++) {
        std::cout << max_vertexes[i] << " ";
    }
    return 0;
}
